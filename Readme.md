# ECS Tools Sample Project
A sample project with a collection of tools to be used with Unity's Entity Component System (ECS).  

## List of tools:
* [Entity and System Tool](https://gitlab.com/unity3d-tools/ecs-tools): An editor window with two pages, a systems and an entities page, see the associated [Readme.md](https://gitlab.com/unity3d-tools/ecs-tools/blob/master/Readme.md) for more details.<sup>[Note 1](#note1)</sup>  
#### Notes:<br><sup>Limitations and plans for the future.</sup>
<a name="note1"><b>Note 1:</b> This will likely be split into different editor windows as each page gains more functionality.</a>