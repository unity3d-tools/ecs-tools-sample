﻿using System;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace ECS_Test_Scripts
{
    [DisableAutoCreation]
    public class TestSystem : ComponentSystem
    {
        private EntityQuery testGroup;
        protected override void OnCreateManager()
        {
            //Get a component group for test components.
            testGroup = GetEntityQuery(ComponentType.ReadWrite<IntTestComponent>(),
                                          ComponentType.ReadWrite<FloatTestComponent>(),
                                          ComponentType.ReadWrite<TestSharedComponent>());
            //Check if any entities exist in the component group if so return.
            if(testGroup.CalculateLength() > 0)
                return;
            //Create a TestGenericSystem<T> component system to make sure they display properly in the editor window.
            World.CreateSystem(typeof(TestGenericSystem<IntTestComponent>), Array.Empty<object>());
        }

        //On each update if a entity with test components exists destroy it.
        protected override void OnUpdate()
        {
            if(testGroup.CalculateLength() > 0)
            {
                Debug.Log("This is being run from menu");
                EntityManager.DestroyEntity(testGroup.ToEntityArray(Allocator.Temp)[0]);
            }
        }
    }

    //Declaration for a generic test system.
    [DisableAutoCreation]
    public class TestGenericSystem<T> : ComponentSystem
        where T : struct, IComponentData
    {
        private EntityQuery testGroup;
        protected override void OnCreateManager()
        {
            testGroup = GetEntityQuery(ComponentType.ReadWrite<T>());
            if(testGroup.CalculateLength() > 0)
                return;
            EntityManager.CreateEntity(ComponentType.ReadWrite<T>());
        }

        //On each update remove one component of the generic type from an entity.
        protected override void OnUpdate()
        {
            if (testGroup.CalculateLength() > 0)
            {
                using (var dataArray = (testGroup.ToComponentDataArray<T>(Allocator.Temp)))
                    Debug.Log(dataArray[0]);
                using (var entityArray = testGroup.ToEntityArray(Allocator.Temp))
                    EntityManager.RemoveComponent<T>(entityArray[0]);
            }
        }
    }

    // Implementation of the generic system TestGenericSystem<T>, using IntTestComponent.
    [DisableAutoCreation]
    public class TestGenericSystem : TestGenericSystem<IntTestComponent> { }
}