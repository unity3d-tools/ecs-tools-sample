﻿using Unity.Entities;
using UnityEngine;

namespace ECS_Test_Scripts
{
    // Component to use with the ConvertToEntity workflow, it adds each of the test components with random data.
    public class ConversionComponent : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new IntTestComponent());
            dstManager.AddComponentData(entity, new FloatTestComponent());
            dstManager.AddSharedComponentData(entity, new TestSharedComponent());
        }
    }
}