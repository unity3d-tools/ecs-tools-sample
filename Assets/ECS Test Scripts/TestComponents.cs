﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

namespace ECS_Test_Scripts
{
    //For testing IsZeroSize property of ComponentType see comment in ComponentTypeListView constructor
    public struct TestSharedComponent : ISharedComponentData
    {
        public int Value;

        public TestSharedComponent(bool useRandomData = true)
        {
            if (useRandomData)
            {
                var random = new Random();
                random.InitState(19);

                Value = random.NextInt();
            }
            else
                Value = default;
        }
    }
    //Component with all int types from Unity.Mathematics
    public struct IntTestComponent : IComponentData
    {
        public int2 Int2;
        public int3 Int3;
        public int4 Int4;

        public int2x2 Int2x2;
        public int2x3 Int2x3;
        public int2x4 Int2x4;

        public int3x2 Int3x2;
        public int3x3 Int3x3;
        public int3x4 Int3x4;

        public int4x2 Int4x2;
        public int4x3 Int4x3;
        public int4x4 Int4x4;

        public IntTestComponent(bool useRandomData = true)
        {
            if (useRandomData)
            {
                var random = new Unity.Mathematics.Random();
                random.InitState(19);
                Int2 = random.NextInt2();
                Int3 = random.NextInt3();
                Int4 = random.NextInt4();

                Int2x2 =  new int2x2(random.NextInt2(), random.NextInt2());
                Int2x3 =  new int2x3(random.NextInt2(), random.NextInt2(), random.NextInt2());
                Int2x4 =  new int2x4(random.NextInt2(), random.NextInt2(), random.NextInt2(), random.NextInt2());
                       
                Int3x2 = new int3x2(random.NextInt3(), random.NextInt3());
                Int3x3 = new int3x3(random.NextInt3(), random.NextInt3(), random.NextInt3());
                Int3x4 = new int3x4(random.NextInt3(), random.NextInt3(), random.NextInt3(), random.NextInt3());

                Int4x2 = new int4x2(random.NextInt4(), random.NextInt4());
                Int4x3 = new int4x3(random.NextInt4(), random.NextInt4(), random.NextInt4());
                Int4x4 = new int4x4(random.NextInt4(), random.NextInt4(), random.NextInt4(), random.NextInt4());
            }
            else
            {
                Int2   = default;
                Int3   = default;
                Int4   = default;
                Int2x2 = default;
                Int2x3 = default;
                Int2x4 = default;
                Int3x2 = default;
                Int3x3 = default;
                Int3x4 = default;
                Int4x2 = default;
                Int4x3 = default;
                Int4x4 = default;
            }
        }
    }

    //Component with all float types from Unity.Mathematics
    public struct FloatTestComponent : IComponentData
    {
        public float2 float2;
        public float3 float3;
        public float4 float4;

        public float2x2 float2x2;
        public float2x3 float2x3;
        public float2x4 float2x4;

        public float3x2 float3x2;
        public float3x3 float3x3;
        public float3x4 float3x4;

        public float4x2 float4x2;
        public float4x3 float4x3;
        public float4x4 float4x4;

        public FloatTestComponent(bool useRandomData = true)
        {
            if (useRandomData)
            {
                var random = new Unity.Mathematics.Random();
                random.InitState(19);

                float2 = random.NextFloat2();
                float3 = random.NextFloat3();
                float4 = random.NextFloat4();

                float2x2 = new float2x2(random.NextFloat2(), random.NextFloat2());
                float2x3 = new float2x3(random.NextFloat2(), random.NextFloat2(), random.NextFloat2());
                float2x4 = new float2x4(random.NextFloat2(), random.NextFloat2(), random.NextFloat2(), random.NextFloat2());

                float3x2 = new float3x2(random.NextFloat3(), random.NextFloat3());
                float3x3 = new float3x3(random.NextFloat3(), random.NextFloat3(), random.NextFloat3());
                float3x4 = new float3x4(random.NextFloat3(), random.NextFloat3(), random.NextFloat3(), random.NextFloat3());

                float4x2 = new float4x2(random.NextFloat4(), random.NextFloat4());
                float4x3 = new float4x3(random.NextFloat4(), random.NextFloat4(), random.NextFloat4());
                float4x4 = new float4x4(random.NextFloat4(), random.NextFloat4(), random.NextFloat4(), random.NextFloat4());
            }
            else
            {
                float2   = default;
                float3   = default;
                float4   = default;
                float2x2 = default;
                float2x3 = default;
                float2x4 = default;
                float3x2 = default;
                float3x3 = default;
                float3x4 = default;
                float4x2 = default;
                float4x3 = default;
                float4x4 = default;
            }
        }
    }
}